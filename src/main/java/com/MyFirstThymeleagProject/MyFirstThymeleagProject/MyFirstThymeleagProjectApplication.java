package com.MyFirstThymeleagProject.MyFirstThymeleagProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstThymeleagProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstThymeleagProjectApplication.class, args);
	}

}
